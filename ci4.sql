-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2022 at 06:05 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci4`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `nama`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Agnes Purnawati', 'Jln. Kartini No. 742, Gorontalo 58668, Sulsel', '1972-07-24 08:55:49', '2022-07-08 07:45:39'),
(2, 'Bambang Soleh Kurniawan', 'Dk. Jakarta No. 5, Administrasi Jakarta Selatan 74211, Bengkulu', '2012-02-17 11:35:54', '2022-07-08 07:45:39'),
(3, 'Ophelia Nurdiyanti', 'Jr. Basoka No. 382, Kotamobagu 84173, Jabar', '1989-03-18 15:02:39', '2022-07-08 07:45:39'),
(4, 'Widya Rahmawati', 'Jr. Kebangkitan Nasional No. 399, Bukittinggi 11111, Jambi', '1978-02-15 15:37:04', '2022-07-08 07:45:39'),
(5, 'Kala Saragih', 'Gg. Bambu No. 952, Banjarbaru 49511, Pabar', '1997-10-10 17:08:35', '2022-07-08 07:45:39'),
(6, 'Ghaliyati Anastasia Wastuti M.Kom.', 'Ds. Raden Saleh No. 675, Sorong 84457, Sulsel', '2020-09-30 03:28:17', '2022-07-08 07:45:39'),
(7, 'Eka Puspasari', 'Jr. Bakau No. 378, Manado 92181, NTB', '1977-12-15 01:53:44', '2022-07-08 07:45:39'),
(8, 'Emil Nainggolan', 'Psr. Panjaitan No. 443, Bandar Lampung 36300, Jatim', '2020-04-25 04:20:08', '2022-07-08 07:45:39'),
(9, 'Aurora Wastuti S.IP', 'Ds. Suprapto No. 947, Bima 47326, NTB', '1975-02-01 07:34:48', '2022-07-08 07:45:40'),
(10, 'Hani Suryatmi S.E.I', 'Ds. Adisucipto No. 920, Bandung 45392, Papua', '1979-10-30 06:34:27', '2022-07-08 07:45:40'),
(11, 'Ida Wulandari S.E.', 'Jln. Yogyakarta No. 989, Blitar 84222, Jambi', '2000-12-23 23:59:32', '2022-07-08 07:45:40'),
(12, 'Ridwan Tamba', 'Dk. Bakin No. 676, Cilegon 57913, Kalbar', '1993-10-03 22:13:54', '2022-07-08 07:45:40'),
(13, 'Adika Thamrin', 'Dk. Taman No. 607, Tanjung Pinang 75410, Jambi', '2010-07-31 10:01:39', '2022-07-08 07:45:40'),
(14, 'Nasrullah Lukita Simbolon', 'Kpg. Supomo No. 237, Jayapura 40168, Aceh', '1975-03-27 12:47:07', '2022-07-08 07:45:40'),
(15, 'Chandra Rahman Januar', 'Ki. Laswi No. 523, Tebing Tinggi 66793, Kepri', '1976-02-17 14:13:04', '2022-07-08 07:45:40'),
(16, 'Bella Astuti', 'Gg. Yogyakarta No. 405, Palembang 95035, Jatim', '2009-09-18 13:27:18', '2022-07-08 07:45:40'),
(17, 'Najwa Hassanah S.IP', 'Ds. Abdullah No. 124, Probolinggo 53257, Maluku', '1995-11-04 07:36:52', '2022-07-08 07:45:40'),
(18, 'Farah Pudjiastuti', 'Ds. Salak No. 881, Magelang 17346, Pabar', '2008-04-13 03:04:38', '2022-07-08 07:45:40'),
(19, 'Imam Hairyanto Gunawan M.TI.', 'Ds. Zamrud No. 269, Jayapura 50797, Sulbar', '2017-04-12 11:36:37', '2022-07-08 07:45:40'),
(20, 'Jarwa Kuswoyo', 'Jln. Labu No. 326, Banjar 34865, DKI', '2013-07-02 00:32:12', '2022-07-08 07:45:40'),
(21, 'Usman Latupono', 'Gg. Raden Saleh No. 742, Binjai 83698, Papua', '1986-12-08 09:37:48', '2022-07-08 07:45:40'),
(22, 'Putri Fujiati', 'Psr. Supomo No. 198, Salatiga 15483, NTT', '1978-03-11 01:04:14', '2022-07-08 07:45:40'),
(23, 'Sakti Tarihoran', 'Ds. Supono No. 96, Dumai 99430, Malut', '2001-02-04 13:10:31', '2022-07-08 07:45:40'),
(24, 'Ellis Gina Purnawati M.Pd', 'Ds. Gotong Royong No. 581, Bukittinggi 13400, Aceh', '2016-07-02 00:37:42', '2022-07-08 07:45:40'),
(25, 'Harjasa Thamrin', 'Gg. Baja Raya No. 735, Gunungsitoli 90137, DKI', '1994-12-05 15:56:53', '2022-07-08 07:45:40'),
(26, 'Gamanto Prakasa', 'Gg. Mulyadi No. 396, Dumai 10346, Sulbar', '1993-08-03 05:39:39', '2022-07-08 07:45:40'),
(27, 'Zulfa Aurora Namaga S.Psi', 'Kpg. Rajawali Timur No. 33, Jayapura 18775, Jabar', '1979-09-28 22:58:18', '2022-07-08 07:45:40'),
(28, 'Melinda Sudiati M.Kom.', 'Psr. R.E. Martadinata No. 583, Metro 58052, Sumut', '1983-02-09 04:00:13', '2022-07-08 07:45:40'),
(29, 'Nilam Puspasari S.E.I', 'Gg. Ters. Kiaracondong No. 791, Tasikmalaya 86992, NTB', '2003-10-25 05:55:20', '2022-07-08 07:45:40'),
(30, 'Zizi Utami', 'Jln. Camar No. 845, Metro 37615, Sumbar', '1982-08-30 23:49:00', '2022-07-08 07:45:40');

-- --------------------------------------------------------

--
-- Table structure for table `auth_activation_attempts`
--

CREATE TABLE `auth_activation_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_groups`
--

CREATE TABLE `auth_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_groups_permissions`
--

CREATE TABLE `auth_groups_permissions` (
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `permission_id` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_groups_users`
--

CREATE TABLE `auth_groups_users` (
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_logins`
--

CREATE TABLE `auth_logins` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `date` datetime NOT NULL,
  `success` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_logins`
--

INSERT INTO `auth_logins` (`id`, `ip_address`, `email`, `user_id`, `date`, `success`) VALUES
(1, '::1', 'yohande90@gmail.com', 1, '2022-07-08 11:56:41', 0),
(2, '::1', 'yohande90@gmail.com', 1, '2022-07-08 12:01:49', 0),
(3, '::1', 'yohan deny', NULL, '2022-07-08 12:31:00', 0),
(4, '::1', 'yohande90@gmail.com', NULL, '2022-07-08 12:31:28', 0),
(5, '::1', 'yohan deny', NULL, '2022-07-08 12:39:21', 0),
(6, '::1', 'napi', NULL, '2022-07-10 01:54:00', 0),
(7, '::1', 'yohande90@gmail.com', 10, '2022-07-10 02:05:23', 0),
(8, '::1', 'yohanesdeny@gmail.com', 11, '2022-07-10 02:15:08', 0),
(9, '::1', 'yohande90@gmail.com', 12, '2022-07-10 07:54:11', 0),
(10, '::1', 'yohan@gmail.com', 13, '2022-07-10 08:05:45', 0),
(11, '::1', 'yohan deny', NULL, '2022-07-10 08:22:04', 0);

-- --------------------------------------------------------

--
-- Table structure for table `auth_permissions`
--

CREATE TABLE `auth_permissions` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_reset_attempts`
--

CREATE TABLE `auth_reset_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_tokens`
--

CREATE TABLE `auth_tokens` (
  `id` int(11) UNSIGNED NOT NULL,
  `selector` varchar(255) NOT NULL,
  `hashedValidator` varchar(255) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `expires` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_users_permissions`
--

CREATE TABLE `auth_users_permissions` (
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `permission_id` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(1, '2022-07-08-103942', 'App\\Database\\Migrations\\Admin', 'default', 'App', 1657277497, 1),
(2, '2017-11-20-223112', 'Myth\\Auth\\Database\\Migrations\\CreateAuthTables', 'default', 'Myth\\Auth', 1657291296, 2);

-- --------------------------------------------------------

--
-- Table structure for table `papercup`
--

CREATE TABLE `papercup` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `stok` varchar(255) NOT NULL,
  `ukuran` varchar(255) NOT NULL,
  `kapasitas` varchar(255) NOT NULL,
  `harga` varchar(255) NOT NULL,
  `koleksi` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `papercup`
--

INSERT INTO `papercup` (`id`, `judul`, `stok`, `ukuran`, `kapasitas`, `harga`, `koleksi`, `created_at`, `updated_at`) VALUES
(1, 'Custom Lucu', '500', 'Tinggi : 9 cm, Diameter Atas :7,4 cm , Diameter Bawah : 5 cm', '220 ml', 'Rp, 10.000', 'papercup1.jpg', '2022-07-05 09:31:01', '2022-07-05 09:31:01'),
(2, 'Basic', '1000', 'Tinggi : 9 cm, Diameter Atas :7,4 cm , Diameter Bawah : 5 cm', '220 ml', 'Rp, 5.000', 'papercup2.jpg', '2022-07-05 09:31:01', '2022-07-05 09:31:01');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jumlah` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `reset_hash` varchar(255) DEFAULT NULL,
  `reset_at` datetime DEFAULT NULL,
  `reset_expires` datetime DEFAULT NULL,
  `activate_hash` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `status_message` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `force_pass_reset` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password_hash`, `reset_hash`, `reset_at`, `reset_expires`, `activate_hash`, `status`, `status_message`, `active`, `force_pass_reset`, `created_at`, `updated_at`, `deleted_at`) VALUES
(14, 'yohan@gmail.com', 'yohan deny', '$2y$10$/BVth5PecXL2B8OG9/aT0eHP.yyrXFyXnDDj63NJ8luvo8vHk8S2m', NULL, NULL, NULL, '2484771934cf34cf0692b3bf6108fc99', NULL, NULL, 0, 0, '2022-07-10 08:21:27', '2022-07-10 08:21:27', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_activation_attempts`
--
ALTER TABLE `auth_activation_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_groups`
--
ALTER TABLE `auth_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_groups_permissions`
--
ALTER TABLE `auth_groups_permissions`
  ADD KEY `auth_groups_permissions_permission_id_foreign` (`permission_id`),
  ADD KEY `group_id_permission_id` (`group_id`,`permission_id`);

--
-- Indexes for table `auth_groups_users`
--
ALTER TABLE `auth_groups_users`
  ADD KEY `auth_groups_users_user_id_foreign` (`user_id`),
  ADD KEY `group_id_user_id` (`group_id`,`user_id`);

--
-- Indexes for table `auth_logins`
--
ALTER TABLE `auth_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `auth_permissions`
--
ALTER TABLE `auth_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_reset_attempts`
--
ALTER TABLE `auth_reset_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auth_tokens_user_id_foreign` (`user_id`),
  ADD KEY `selector` (`selector`);

--
-- Indexes for table `auth_users_permissions`
--
ALTER TABLE `auth_users_permissions`
  ADD KEY `auth_users_permissions_permission_id_foreign` (`permission_id`),
  ADD KEY `user_id_permission_id` (`user_id`,`permission_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `papercup`
--
ALTER TABLE `papercup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `auth_activation_attempts`
--
ALTER TABLE `auth_activation_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_groups`
--
ALTER TABLE `auth_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_logins`
--
ALTER TABLE `auth_logins`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `auth_permissions`
--
ALTER TABLE `auth_permissions`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_reset_attempts`
--
ALTER TABLE `auth_reset_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `papercup`
--
ALTER TABLE `papercup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_groups_permissions`
--
ALTER TABLE `auth_groups_permissions`
  ADD CONSTRAINT `auth_groups_permissions_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `auth_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `auth_groups_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `auth_permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `auth_groups_users`
--
ALTER TABLE `auth_groups_users`
  ADD CONSTRAINT `auth_groups_users_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `auth_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `auth_groups_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  ADD CONSTRAINT `auth_tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `auth_users_permissions`
--
ALTER TABLE `auth_users_permissions`
  ADD CONSTRAINT `auth_users_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `auth_permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `auth_users_permissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
